<?php

defined('_JEXEC') or die('Restricted access');
require_once JPATH_ROOT .'/components/com_community/libraries/core.php';

class plgCommunityGit_Repositories extends CApplications
{
    protected $_myRequest = false;

    function plgCommunityGit_Repositories(& $subject, $config)
    {
        parent::__construct($subject, $config);

        if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
            $application = JFactory::getApplication();
            $jInput = $application->input;


            if(isset($_POST['featured_repo']) and $_POST['featured_repo'] == 'true'){
                $user = JFactory::getUser();
                //If User Not Logged In Return false.
                if(!$user->id){
                    return false;
                }

                //Check if Repository ID has been provided
                if(isset($_POST['repo_id'])){
                    $repositoryID = $_POST['repo_id'];
                }
                //Well We Need to Toggle the featured Repo.
                //First get the current value from the database.
                $db = JFactory::getDbo();
                $idQuery = $db->getQuery(true);
                $idQuery
                    ->select($db->quoteName(array('id','isFeatured')))
                    ->from($db->quoteName('#__cts_git_repositories'))
                    ->where($db->quoteName('isPublic') . ' = 1')
                    ->where($db->quoteName('user_id') . ' = '.$db->quote($user->id))
                    ->where($db->quoteName('id') . ' = '.$db->quote($repositoryID));
                // Reset the query using our newly populated query object.
                $db->setQuery($idQuery);
                //Get the Results
                $repository = $db->loadObject();

                if(!empty($repository)){
                    $repositoryUpdated = new stdClass();
                    if($repository->isFeatured == '1'){
                        $repositoryUpdated->isFeatured = 0;
                    }else{
                        $repositoryUpdated->isFeatured = 1;
                    }
                    $repositoryUpdated->id = $repository->id;
                    //Finally Update the Record
                    $result = JFactory::getDbo()->updateObject('#__cts_git_repositories', $repositoryUpdated, 'id');
                    if($result === true){
                        echo 'true';
                    }
                    exit;
                }
            }

            if(isset($_POST['visible_repo']) and $_POST['visible_repo'] == 'true'){
                $user = JFactory::getUser();
                //If User Not Logged In Return false.
                if(!$user->id){
                    return false;
                }

                //Check if Repository ID has been provided
                if(isset($_POST['repo_id'])){
                    $repositoryID = $_POST['repo_id'];
                }
                //Well We Need to Toggle the Visibility of Repo.
                //First get the current value from the database.
                $db = JFactory::getDbo();
                $idQuery = $db->getQuery(true);
                $idQuery
                    ->select($db->quoteName(array('id','doShow')))
                    ->from($db->quoteName('#__cts_git_repositories'))
                    ->where($db->quoteName('isPublic') . ' = 1')
                    ->where($db->quoteName('user_id') . ' = '.$db->quote($user->id))
                    ->where($db->quoteName('id') . ' = '.$db->quote($repositoryID));
                // Reset the query using our newly populated query object.
                $db->setQuery($idQuery);
                //Get the Results
                $repository = $db->loadObject();

                if(!empty($repository)){
                    $repositoryUpdated = new stdClass();
                    if($repository->doShow == '1'){
                        $repositoryUpdated->doShow = 0;
                    }else{
                        $repositoryUpdated->doShow = 1;
                    }
                    $repositoryUpdated->id = $repository->id;
                    //Finally Update the Record
                    $result = JFactory::getDbo()->updateObject('#__cts_git_repositories', $repositoryUpdated, 'id');
                    if($result === true){
                        echo 'true';
                    }
                    exit;
                }
            }
        }
    }

    function onProfileDisplay()
    {

        ob_start();

        $application = JFactory::getApplication();
        $jInput = $application->input;
        $userID = $jInput->get('userid');

        //lets get the loggedIn User
        $user = JFactory::getUser();        // Get the user object
        if($user->id){
            $loggedInUserID = $user->id;
        }else{
            $loggedInUserID = false;
        }

        //Lets Fetch the Repositories for the User if there is any.
        $profileHTML = [];
        $profileHTML['tab'] = [
            'class' => '',
            'text' => 'Repositories',
            'href' => '#joms-app--repos-special'
        ];

        $data = [];


        $pDB = JFactory::getDbo();
        $idQuery = $pDB->getQuery(true);
        $idQuery
            ->select($pDB->quoteName(array('id','gitService','name','doShow','isFeatured','url','description')))
            ->from($pDB->quoteName('#__cts_git_repositories'))
            ->where($pDB->quoteName('isPublic') . ' = 1');

        //If LoggedIn UserID
        if(!isset($loggedInUserID)){
            $idQuery->where($pDB->quoteName('doShow') . ' = 1');
        }

        $idQuery->where($pDB->quoteName('user_id') . ' = '.$pDB->quote($userID))
            ->order('id ASC');
        // Reset the query using our newly populated query object.
        $pDB->setQuery($idQuery);
        //Get the Results
        $repositories = $pDB->loadObjectList();

        ?>
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-12">
                    <h2>Public Repositories</h2>
                </div>

                <div class="col-md-12">
                    <table id="git_repositories-list">
                        <thead><tr>
                            <th>Source</th>
                            <th>Name</th>
                            <th>Description</th>
                            <?=($loggedInUserID)?'<th>Actions</th>':'<th>Label</th>'?>
                        </tr></thead>
                        <tbody>
                        <?php
                        if(!empty($repositories) and is_array($repositories)){
                            foreach($repositories as $repository){
                                echo '<tr data-id="'.$repository->id.'">';
                                echo "<td>".$repository->gitService."</td>";
                                echo "<td><a href='".$repository->url."'>".$repository->name."</a></td>";
                                echo "<td>".$repository->description."</td>";
                                if($loggedInUserID>0){
                                    echo "<td>
                                            <a href='javascript:void(0)'><i class='visible-repo fa ".(($repository->doShow == 1)?"fa-eye":"fa-eye-slash")."'></i></a>
                                            <a href='javascript:void(0)'><i class='featured-repo fa ".(($repository->isFeatured == 1)?"fa-star":"fa-star-o")."'></i></a>
                                         </td>";
                                }
                                else{
                                    if($repository->isFeatured == '1'){
                                        echo '<td><span class="featured">Featured</span></td>';
                                    }else{
                                        echo '<td></td>';
                                    }
                                }
                                echo '</tr>';
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <?php      echo '<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
              <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.13/datatables.min.js"></script>';

        ?>
        <style type="text/css">
            .featured-repo:hover{
                color: #ee730a;
            }
            .fa-star{
                color: #ee730a;
            }
            .fa-star:hover{
                color: #4c6db5;
            }
            .fa-eye-slash{
                color: #a50700;
            }
            .fa-eye-slash:hover{
                color: #1b8700;
            }
            .fa-eye{
                color: #1b8700;
            }
            .fa-eye:hover{
                color: #a50700;
            }
            span.featured{
                background: red;
                color: white;
                padding: 4px 10px;
                font-size: small;
                border-radius: 3px;
            }
        </style>
        <script type="text/javascript">
            jQuery(function($){
                $('#git_repositories-list').DataTable();

                //on star click send ajax request
                $('.featured-repo').on('click',function () {
                    var clickedRowID = $(this).closest('tr').attr('data-id');
                    var clickedObj = $(this);
                    $.ajax({
                        url:"index.php",
                        data:{'featured_repo':'true','repo_id':clickedRowID},
                        type:"POST",
                        success:function (output) {
                            if(output=='true'){
                                var classes = clickedObj.attr('class');
                                if(classes.indexOf('fa-star-o') >= 0 ){
                                    clickedObj.removeClass('fa-star-o');
                                    clickedObj.addClass('fa-star')
                                }else{
                                    clickedObj.removeClass('fa-star');
                                    clickedObj.addClass('fa-star-o')
                                }
                            }
                        }
                    });
                });//End of On Click Function for Featured Repo.

                //on eye click send ajax request
                $('.visible-repo').on('click',function () {
                    var clickedRowID = $(this).closest('tr').attr('data-id');
                    var clickedObj = $(this);
                    $.ajax({
                        url:"index.php",
                        data:{'visible_repo':'true','repo_id':clickedRowID},
                        type:"POST",
                        success:function (output) {
                            if(output=='true'){
                                var classes = clickedObj.attr('class');
                                if(classes.indexOf('fa-eye-slash') >= 0 ){
                                    clickedObj.removeClass('fa-eye-slash');
                                    clickedObj.addClass('fa-eye')
                                }else{
                                    clickedObj.removeClass('fa-eye');
                                    clickedObj.addClass('fa-eye-slash')
                                }
                            }
                        }
                    });
                });//End of On Click Function for Featured Repo.
            });
        </script>

        <?php
        $content	= ob_get_contents();
        ob_end_clean();

        return $content;
    }
}
